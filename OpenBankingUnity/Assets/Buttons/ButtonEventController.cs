using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class ButtonEventController : MonoBehaviour
{
    [Header("Begin")]
    public UnityEvent onBegin;
    [Header("End")]
    public UnityEvent onEnd;
    [Header("Pressed")]
    public UnityEvent onPressed;
    [Header("Released")]
    public UnityEvent onReleased;
    public KeyBoardController keyBoardController;

    public Material SphereMaterial1;
    public Material SphereMaterial2;
    public Material SphereMaterial3;
    public Material SphereMaterial4;
    public Material SphereMaterial5;
    
    void Start()
    {
        MeshRenderer meshRenderer = GetComponent<MeshRenderer>();
        SphereMaterial1 = meshRenderer.material;
    }

    public void Begin(){
        //MeshRenderer meshRenderer = GetComponent<MeshRenderer>();
        //meshRenderer.material = SphereMaterial2;
        onBegin.Invoke();
    }
    
    public void End(){
        MeshRenderer meshRenderer = GetComponent<MeshRenderer>();
        meshRenderer.material = SphereMaterial1;
        onEnd.Invoke();
    }
    
    public void Pressed(){
        MeshRenderer meshRenderer = GetComponent<MeshRenderer>();
        meshRenderer.material = SphereMaterial2;
        onPressed.Invoke();
        OnClick();
    }
    
    public void Released(){
        MeshRenderer meshRenderer = GetComponent<MeshRenderer>();
        meshRenderer.material = SphereMaterial1;
        onReleased.Invoke();
    }
    
    public void OnClick()
    {
        string s = gameObject.GetComponent<ButtonController>().GetText();
        if(keyBoardController!=null)
            keyBoardController.OnClick(s);
    }
}
