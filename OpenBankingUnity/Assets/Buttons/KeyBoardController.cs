using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyBoardController : MonoBehaviour
{
    private List<string> keyboard1 = new List<string>();
    private List<string> keyboard2 = new List<string>();
    private Dictionary<int, GameObject> buttons = new Dictionary<int, GameObject>();
    private bool mayus = false;
    public LoginController loginController;
     
    // Start is called before the first frame update
    void Start()
    {
        keyboard1.Add("abcdefghijk");
        keyboard1.Add("lmnopqrstub");
        keyboard1.Add("wxyz@_-=. «");

        keyboard2.Add("ABCDEFGHIJK");
        keyboard2.Add("LMNOPQRSTUB");
        keyboard2.Add("WXYZ@_-=. »");

        ChangeMayus();
    }

    private void ChangeMayus(){
        mayus = !mayus;
        if(mayus){
            CreateKeyboard(keyboard2);
        } else {
            CreateKeyboard(keyboard1);
        }
    }

    private void CreateKeyboard(List<string> keyboard){
        int counter = 0;
        for (int i = 0; i < keyboard.Count; i++)
        {
            for (int j = 0; j < keyboard[i].Length; j++)
            {
                var pos = transform.position;
                var posLabel = pos - transform.up * (i*0.04f) + transform.right * (j*0.04f-0.2f);
                GameObject go;
                if(!this.buttons.ContainsKey(counter)){
                    Debug.Log("Create");
                    go = Instantiate(Resources.Load<GameObject>("Buttons/Button"), posLabel, Quaternion.identity);
                    go.GetComponent<ButtonEventController>().keyBoardController = this;
                    go.transform.rotation = transform.rotation;
                    go.transform.SetParent(transform);
                    buttons.Add(counter, go);
                } else {
                    Debug.Log("Bring");
                    go = this.buttons[counter];
                }
                go.GetComponent<ButtonController>().SetText(keyboard[i][j]+"");
                counter++;
            }
        }
    }

    public void OnClick(string v)
    {
        v = v+"";
        Debug.Log(v);
        if((string.Compare(v, "«") == 0) || (string.Compare(v, "»") == 0)){
            ChangeMayus();
        } else {
            loginController.OnClick(v);
        }
    }
}
