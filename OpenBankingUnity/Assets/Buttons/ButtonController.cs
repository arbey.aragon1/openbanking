using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonController : MonoBehaviour
{
    public TMPro.TextMeshProUGUI labelLanguage;

    public void SetText(string s)
    {
        labelLanguage.text = s;
    }
    public string GetText()
    {
        return labelLanguage.text;
    }
}
