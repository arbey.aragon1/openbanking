using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoginController : MonoBehaviour
{
    private string username = "";
    private string password = "";

    public ButtonController email;
    public ButtonController pass;
    private bool emailSelected = true;
    public GameController gameController;

    
    
    void Start()
    {
        emailSelected = true;
    }

    public void ClearAll(){
        username = ""; 
        email.SetText("");
        password = "";
        pass.SetText("");
        emailSelected = true;
    }

    public void OnEmail()
    {
        emailSelected = true;
    }

    public void OnPass()
    {
        emailSelected = false;
    }

    public void OnSend()
    {
        gameController.OnSendUserAndPass(username, password);
    }

    public void OnClear()
    {
        if(emailSelected){
            username = ""; 
            email.SetText("");
        } else {
            password = "";
            pass.SetText("");
        }
    }

    public void OnClick(string c){
        Debug.Log("---");
        if(emailSelected){
            username += c; 
            email.SetText(username);
        } else {
            password += c;
            string passShow = "";
            for (int i = 0; i < password.Length; i++)
            {
                passShow += "*";
            }
            pass.SetText(passShow);
        }
    }

    public void SetActive(bool v){
        gameObject.SetActive(v);
    }
}
