using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Windows.Speech;
public class VoiceCommandsController : MonoBehaviour
{
    KeywordRecognizer keywordRecognizer = null;
    Dictionary<string, System.Action> keywords = new Dictionary<string, System.Action>();

    private bool enableVoiceCommands = false;

    public GameController gameController;

    void Start()
    {
        keywords.Add("hola", () =>
        {
            Commands("com1");
        });

        keywords.Add("ver el menu", () =>
        {
            Commands("com2");
        });

        keywords.Add("ver los proveedores", () =>
        {
            Commands("com3");
        });

        keywords.Add("ver mis cuentas", () =>
        {
            Commands("com4");
        });

        keywords.Add("ver estadisticas", () =>
        {
            Commands("com5");
        });

        keywords.Add("ver recomendaciones", () =>
        {
            Commands("com6");
        });

        keywords.Add("salir", () =>
        {
            Commands("com7");
        });

        keywords.Add("no", () =>
        {
            Commands("com8");
        });

        keywords.Add("ver referencias", () =>
        {
            Commands("com9");
        });
        keywordRecognizer = new KeywordRecognizer(keywords.Keys.ToArray());
        keywordRecognizer.OnPhraseRecognized += KeywordRecognizer_OnPhraseRecognized;
        keywordRecognizer.Start();
    }
   

    private void KeywordRecognizer_OnPhraseRecognized(PhraseRecognizedEventArgs args)
    {
        System.Action keywordAction;
        if (keywords.TryGetValue(args.text, out keywordAction))
        {
            keywordAction.Invoke();
        }
    }

    public void Commands(string comm){
        if(comm == "com1"){

        } else if(comm == "com2"){

        } else if(comm == "com3"){
            gameController.ShowMap();
        } else if(comm == "com4"){

        } else if(comm == "com5"){

        } else if(comm == "com6"){

        } else if(comm == "com7"){

        } else if(comm == "com8"){
            
        } else if(comm == "com9"){
            
        }
    }

    public void EnableVoiceCommand(bool v){
        this.enableVoiceCommands = v;
    }
}
