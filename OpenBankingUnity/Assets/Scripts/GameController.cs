using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using System.Threading.Tasks;
using SimpleJSON;
using System.Security.Cryptography;
public class GameController : MonoBehaviour
{
    private DataUser _prepstore;
    public enum Status { WaitingFirebasePass, WaitingPrometeoPass, EnterToHome}
    private bool continueWaiting = false; 
    private Action<string> callbackTimer = (string x)=>{};
    private int timeLimit = 1;
    float timer = 0.0f;
    public AudioController audioController;
    public AvatarController avatarController;
    public LoginController loginController;
    public MapController mapController;
    private Status status;
    void Start()
    {
        
        StartConversation();
    }

    void Update()
    {
        if(continueWaiting){
            timer += Time.deltaTime;
            int seconds = (int)(timer % 60);
            if(timeLimit < seconds){
                continueWaiting = false;
                callbackTimer("");
            }
        }  
    }

    private void SetTime(int time, Action<string> callback){
        timer = 0.0f;
        timeLimit = time;
        callbackTimer = callback;
        continueWaiting = true;
    }

    void StartConversation(){
        avatarController.SetActive(false);
        loginController.SetActive(false);
        SetTime(3, (string s)=>{
            avatarController.SetActive(true);
            avatarController.UpdateStatus(500f, 2000f, AvatarController.GetStatusValue("saludo1"));
            audioController.LoadAudioClip("vo2");
            audioController.Play((string s1)=>{
                status = Status.WaitingFirebasePass;
                loginController.SetActive(true);
            });
        });
    }

    public void OnSendUserAndPass(string username, string password){
        if(status == Status.WaitingFirebasePass){
            loginController.SetActive(false);
            avatarController.UpdateStatus(500f, 3000f, AvatarController.GetStatusValue("abrir"));
            SetTime(3, (string s)=>{
                audioController.LoadAudioClip("vo3");
                audioController.Play((string s1)=>{
                    status = Status.WaitingPrometeoPass;
                    loginController.SetActive(true);

                });
            });
        } else if(status == Status.WaitingPrometeoPass){
            loginController.SetActive(false);
            avatarController.UpdateStatus(500f, 3000f, AvatarController.GetStatusValue("abrir"));
            SetTime(3, (string s)=>{
                audioController.LoadAudioClip("vo4");
                audioController.Play((string s1)=>{
                    status = Status.EnterToHome;
                    ShowMap();

                });
            });
        }

    }

    public void ShowMap(){
        if(this._prepstore!=null){
            mapController.SetActive(true);
            mapController.ConfigureTooltips(this._prepstore.providers);
        }
    }

    public void DataLoaded(DataUser prepstore){
        this._prepstore = prepstore;
    }
}
