using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using System.Threading.Tasks;
using SimpleJSON;
using System.Security.Cryptography;
using System.Runtime.InteropServices;
using System.Text;

public class CommunicationService
{
    private Main _main;
    private AuthService _authService;
    private MicroService _microService;
    private AsymmetricEncryptionService _asymmetricEncryptionService;
    private SymmetricEncryptionService _symmetricEncryptionService;
    private DataUser prepstore;


    public CommunicationService(AuthService authService, 
                                MicroService microService, 
                                AsymmetricEncryptionService asymmetricEncryptionService, 
                                SymmetricEncryptionService symmetricEncryptionService, 
                                Main main) { 
        _main = main;
        _authService = authService;
        _microService = microService;
        _asymmetricEncryptionService = asymmetricEncryptionService;
        _symmetricEncryptionService = symmetricEncryptionService;

        string user = "test@test.com";
        string pass = "0123456789";
        
        this.AuthFirebase(user, pass);

        //Receive public key


        //Encrypted and send symmetric key


        //Send hello world
    }

    public void AuthFirebase(string user, string pass) {
        Debug.Log("**AuthFirebase");
        //Authentication with firebase and receive token
        _authService.Login(user, pass, (string x) => {
            this.StartConnection();
        });
    }

    public void StartConnection() {
        Debug.Log("**StartConnection");
        //Request to start connection
        _microService.SendMessage("START_CONNECTION", "",(string v)=>{    
            _microService.ReadResponse((string x)=>{
                if(x != null){
                    var objects = JSON.Parse(x);
                    foreach(var root in objects)
                    {
                        //Debug.Log(root);
                        string[] subs = root.ToString().Trim(new Char[] {'[', ']'}).Split(',', '"');
                        
                        string key = subs[0];
                        string publicKey = subs[4];

                        _asymmetricEncryptionService.SetPublicKey(publicKey);

                        Debug.Log(publicKey);
                        break;
                    }
                    _microService.DeleteResponse((string x)=>{
                        this.GenerateKeyAndEncript();
                    });
                    
                }
            });
        });
    }

    public void GenerateKeyAndEncript(){
        Debug.Log("**GenerateKeyAndEncript");
        string symmetricKey = _symmetricEncryptionService.GetKey();
        string symmetricKeyEncrypted = _asymmetricEncryptionService.Encrypt(symmetricKey);
        //Send symmetric key
        _microService.SendMessage("SYNC_KEYS", symmetricKeyEncrypted,(string v)=>{ 
            Debug.Log("**ReadResponse 0");
            _microService.ReadResponse((string x)=>{
                Debug.Log("**ReadResponse 1");
                if(x != null){
                    var objects = JSON.Parse(x);
                    foreach(var root in objects)
                    {
                        Debug.Log(root);
                        string[] subs = root.ToString().Trim(new Char[] {'[', ']'}).Split(',', '"');
                        
                        string key = subs[0];
                        string encryptedMessage = subs[4];

                        string msg = _symmetricEncryptionService.Decrypt(encryptedMessage);

                        Debug.Log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                        Debug.Log(msg);
                        this.prepstore = JsonUtility.FromJson<DataUser>(msg);

                        _main.gameController.DataLoaded(this.prepstore);

                        break;
                    }
                    _microService.DeleteResponse((string v)=>{});/**/
                }
            });
        });
    }


}


[System.Serializable]
public class DataUser
{
    public string status;
    public List<Provider> providers;
    public List<Account> accounts;
}

[System.Serializable]
public class Provider{
    public string code;
    public string name;
    public string country;
}

[System.Serializable]
public class Account{
    public int id;
    public string name;
    public int number;
    public string branch;
    public string currency;
    public int balance;
    public List<Movement> movements;
}

[System.Serializable]
public class Movement{
    public int id;
    public string reference;
    public string date;
    public string detail;
    public float debit;
    public float credit;
    public string extra_data;
}