
using System;
using System.Collections.Generic;

using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

public partial class LoginModel
{
    [JsonProperty("kind", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public string Kind { get; set; }

    [JsonProperty("localId", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public string LocalId { get; set; }

    [JsonProperty("email", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public string Email { get; set; }

    [JsonProperty("displayName", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public string DisplayName { get; set; }

    [JsonProperty("idToken", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public string IdToken { get; set; }

    [JsonProperty("registered", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public bool? Registered { get; set; }

    [JsonProperty("refreshToken", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public string RefreshToken { get; set; }

    [JsonProperty("expiresIn", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    [JsonConverter(typeof(ParseStringConverter))]
    public long? ExpiresIn { get; set; }

    [JsonProperty("error", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public LoginModelError Error { get; set; }
}

public partial class LoginModelError
{
    [JsonProperty("code", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public long? Code { get; set; }

    [JsonProperty("message", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public string Message { get; set; }

    [JsonProperty("errors", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public List<ErrorElement> Errors { get; set; }
}

public partial class ErrorElement
{
    [JsonProperty("message", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public string Message { get; set; }

    [JsonProperty("domain", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public string Domain { get; set; }

    [JsonProperty("reason", Required = Required.DisallowNull, NullValueHandling = NullValueHandling.Ignore)]
    public string Reason { get; set; }
}

public partial class LoginModel
{
    public static LoginModel FromJson(string json) => JsonConvert.DeserializeObject<LoginModel>(json);
}

public static class Serialize
{
    public static string ToJson(this LoginModel self) => JsonConvert.SerializeObject(self);
}

internal static class Converter
{
    public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
    {
        MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
        DateParseHandling = DateParseHandling.None,
        Converters =
        {
            new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
        },
    };
}

internal class ParseStringConverter : JsonConverter
{
    public override bool CanConvert(Type t) => t == typeof(long) || t == typeof(long?);

    public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
    {
        if (reader.TokenType == JsonToken.Null) return null;
        var value = serializer.Deserialize<string>(reader);
        long l;
        if (Int64.TryParse(value, out l))
        {
            return l;
        }
        throw new Exception("Cannot unmarshal type long");
    }

    public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
    {
        if (untypedValue == null)
        {
            serializer.Serialize(writer, null);
            return;
        }
        var value = (long)untypedValue;
        serializer.Serialize(writer, value.ToString());
        return;
    }

    public static readonly ParseStringConverter Singleton = new ParseStringConverter();
}

