using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using System.Threading.Tasks;
using SimpleJSON;
public class MicroService
{   
    private Main _main;
    private AuthService _authService;
    
    public MicroService(AuthService authService, Main main) { 
        _main = main;
        _authService = authService;
    }

    public void SendMessage(string command, string msg, Action<string> callback){
        string path = "ms-auth";
        string requestBodyJsonString = "{\"token\" : \""+_authService.GetToken()+"\", \"uid\" : \""+_authService.GetUID()+"\", \"command\" : \""+command+"\", \"msg\" : \""+msg+"\"}";
        string method = "POST";
        string url = $"{Constants.databaseURL}{path}.json?auth={_authService.GetToken()}";
        _main.CreateRequestAsync(url, requestBodyJsonString, method, (string x) => {
            Debug.Log(x);
            callback("");
        });
    }

    public void ReadResponse(Action<string> callback)
    {
        Debug.Log("**ReadResponse 0.1");
        _main.SetTimeIntervalAsync(3,3,(string v)=>{
            string path = $"ms-auth-response/{_authService.GetUID()}";
            string method = "GET";
            string url = $"{Constants.databaseURL}{path}.json?auth={_authService.GetToken()}";
            string requestBodyJsonString = "";
            _main.CreateRequestAsync(url, requestBodyJsonString, method, (string x) => {
                callback(x);
            });

        });
    }
    public void DeleteResponse(Action<string> callback)
    {
        string path = $"ms-auth-response/{_authService.GetUID()}";
        string method = "DELETE";
        string url = $"{Constants.databaseURL}{path}.json?auth={_authService.GetToken()}";
        string requestBodyJsonString = "{}";
        _main.CreateRequestAsync(url, requestBodyJsonString, method, callback);
    }


}


