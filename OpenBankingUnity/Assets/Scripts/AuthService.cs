using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using System.Threading.Tasks;
using SimpleJSON;

public class AuthService
{
    private string idToken;
    private string refreshToken;
    private string localId;
    private bool logged = false;
    
    private AuthService _instance;
    private Main _main;

    public AuthService(Main main) { 
        _main = main;
    }

    public void CreateUser(){
        string requestBodyJsonString = "{'email':'test@test.com','password':'0123456789', 'returnSecureToken':true}";
        string method = "POST";
        string url = $"{Constants.mainURL}accounts:signUp?key={Constants.apiKey}";
        Action<String> callback = (string x) => Debug.Log(x);
        _main.CreateRequestAsync(url, requestBodyJsonString, method, callback);
    }

    public void Login(string user, string pass, Action<string> callback){
        this.logged = true;
        string requestBodyJsonString = "{'email':'"+user+"','password':'"+pass+"', 'returnSecureToken':true}";
        string method = "POST";
        string url = $"{Constants.mainURL}accounts:signInWithPassword?key={Constants.apiKey}";
        Action<string> callback1 = (string x) => {
            Debug.Log(x);
            LoginResponse obj =
                JsonUtility.FromJson<LoginResponse>(x);
            idToken = obj.idToken;
            refreshToken = obj.refreshToken;
            localId = obj.localId;
            Debug.Log("-*-*-*-*-*-*");
            callback("");
        };

        _main.CreateRequestAsync(url, requestBodyJsonString, method, callback1);
    }

    public string GetToken(){
        return idToken;
    }

    public string GetUID(){
        return localId;
    }
}

public class LoginResponse
{
    public string idToken;
    public string email;
    public string refreshToken;
    public string expiresIn;
    public string localId;
    public bool registered; 
}
