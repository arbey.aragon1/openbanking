using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using System.Threading.Tasks;
using SimpleJSON;
using System.Security.Cryptography;


public class Main : MonoBehaviour
{
    private Module _module = null;
    private bool continueWaiting = false; 
    private int timeLimit = 1;
    float timer = 0.0f;
    private Action<string> callbackTimer = (string x)=>{};
    private Action<string> callbackRequest = (string x)=>{};
    private AuthService _authService;
    private MicroService _microService;
    public MapController mapController;
    public GameController gameController;
    
    void Start()
    {
        _module = Module.GetInstance(gameObject.GetComponent<Main>());
        _authService = _module.AuthService();
        _microService = _module.MicroService();
    }

     void Update()
    {
        if(continueWaiting){
            timer += Time.deltaTime;
            int seconds = (int)(timer % 60);
            if(timeLimit < seconds){
                continueWaiting = false;
                Debug.Log("**ReadResponse 0.4");
                callbackTimer("");
            }
        }
    }

    public void CreateRequestAsync(string url, string requestBodyJsonString, string method, Action<string> callback){
        callbackRequest = callback;
        StartCoroutine(CreateRequest(url, requestBodyJsonString, method));
    }

    private void RunCallbackRequest(string x){
        callbackRequest(x);
    }

    public IEnumerator CreateRequest(string url, string requestBodyJsonString, string method)
    {
        Debug.Log(method);
        UnityWebRequest request;
        
        switch (method)
        {
            case "POST":
            case "PATCH":
            // Defaults are fine for PUT
            case "PUT":
                byte[] bytes = new System.Text.UTF8Encoding().GetBytes(requestBodyJsonString);
                request = UnityWebRequest.Put(url, bytes);
                request.SetRequestHeader("X-HTTP-Method-Override", method);
                request.SetRequestHeader("accept", "application/json; charset=UTF-8");
                request.SetRequestHeader("content-type", "application/json; charset=UTF-8");
                break;
            case "GET":
                // Defaults are fine for GET
                request = UnityWebRequest.Get(url);
                break;
            case "DELETE":
                // Defaults are fine for DELETE
                request = UnityWebRequest.Delete(url);
                break;
            default:
                throw new Exception("Invalid HTTP Method");
        }

        yield return request.SendWebRequest();

        if (request.isNetworkError)
        {
            Debug.Log("Error While Sending: " + request.error);
        }
        else
        {
            if(method != "DELETE"){
                this.RunCallbackRequest(request.downloadHandler.text);
            } else {
                this.RunCallbackRequest("");
            }
            
        }
        yield return null;
    }

    public void SetTimeIntervalAsync(float delay, int steps, Action<string> callback)
    {
        Debug.Log("**ReadResponse 0.2");
        SetTimeInterval(delay, steps, callback);
        //StartCoroutine();
    }

    private void SetTimeInterval(float delay, int steps, Action<string> callback)
    {
        callbackTimer = callback;
        timer = 0.0f;
        continueWaiting = true;
        timeLimit =  (int) delay;
        Debug.Log("**ReadResponse 0.3");
        Debug.Log(delay);
        //yield return new WaitForSeconds(delay);
        
        //callback("");
        
        /*for(int i = 0; i < steps; i++){
            callback("");
            if(continueWaiting){
                yield return new WaitForSeconds(delay);
            }
        }/**/
        //yield return null;
    }

}
