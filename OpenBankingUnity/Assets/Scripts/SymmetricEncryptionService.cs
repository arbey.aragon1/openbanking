using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using System.Threading.Tasks;
using SimpleJSON;
using System.Security.Cryptography;
using System.Runtime.InteropServices;

public class SymmetricEncryptionService : MonoBehaviour
{
    private Main _main;
    private AuthService _authService;
    private string _key =  AesGcm256.toHex(AesGcm256.NewIv());

    public SymmetricEncryptionService(AuthService authService, Main main) { 
        _main = main;
        _authService = authService;            
    }

    public string Encrypt(string plainText){
        return CryptoUtility.Encrypt(plainText, _key);
    }

    public string Decrypt(string encrypted){
        return CryptoUtility.Decrypt(encrypted, _key);
    }

    public string GetKey(){
        return _key;
    }

    
}
