using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapController : MonoBehaviour
{
    public Transform center;
    public List<GameObject> counties; 
    
    public void ConfigureTooltips(List<Provider> providers){
        Dictionary<string, List<Provider>> dic = new Dictionary<string, List<Provider>>();
        for (int i = 0; i < providers.Count; i++)
        {
            if(!dic.ContainsKey(providers[i].country)){
                dic.Add(providers[i].country, new List<Provider>());
            }
        }
        for (int i = 0; i < providers.Count; i++)
        {
            dic[providers[i].country].Add(providers[i]);
        }
        foreach(var key in dic.Keys)
        {
            SelectByName(key, dic[key]);
        }
    }

    private void SelectByName(string country, List<Provider> providers)
    {
        for (var i = 0; i < counties.Count; i++)
        {
            if(country == counties[i].name){
                
                SetTooltip(i, providers);
            }
        }
    }

    private void SetTooltip(int index, List<Provider> providers){
        TooltipController tooltipController = counties[index].AddComponent(typeof(TooltipController)) as TooltipController;
        tooltipController.center = center;
        tooltipController.origin = counties[index].transform;
        List<string> data = new List<string>();
        for (int i = 0; i < providers.Count; i++)
        {
            data.Add(providers[i].name);
        }
        tooltipController.ConfigureTooltip(data);
    }

    public void SetActive(bool val){
        this.gameObject.SetActive(val);
    }

}
