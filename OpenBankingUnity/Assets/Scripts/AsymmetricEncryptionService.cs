using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using System.Threading.Tasks;
using SimpleJSON;
using System.Security.Cryptography;
using System.Runtime.InteropServices;
using System.Text;

public class AsymmetricEncryptionService : MonoBehaviour
{
    private Main _main;
    private AuthService _authService;
    private string _key =  AesGcm256.toHex(AesGcm256.NewIv());
    private RSACryptoServiceProvider _RSAalg = new RSACryptoServiceProvider(512);
    private ASCIIEncoding _ByteConverter = new ASCIIEncoding();
    private string _publicKey;

    // get private and public key
    public AsymmetricEncryptionService(AuthService authService, Main main) { 
        _main = main;
        _authService = authService; 
        _RSAalg.PersistKeyInCsp = false;    
        

        /*try {
            string dataString = "Arbey";
            string o = Encrypt(dataString);
            string o2 = Decrypt(o);

            Debug.Log("-------------------------------------------");
            Debug.Log(dataString);
            Debug.Log(o);
            Debug.Log(o2);


        }
        catch(ArgumentNullException) {
            Debug.Log("error in data en-/decryption");
        }/**/
    }

    public void SetPublicKey(string publicKey)
    {
        _publicKey = publicKey;
    }

    public string Encrypt(string dataString)
    {
            // Create byte arrays to hold original, encrypted, and decrypted data.
            byte[] originalData = _ByteConverter.GetBytes(dataString);
            byte[] encryptedData;

            // encrypt with xml-public key
            _RSAalg.FromXmlString(_publicKey);
            encryptedData = _RSAalg.Encrypt(originalData, false);
            string encryptedDataBase64 = Convert.ToBase64String(encryptedData);
            
            return encryptedDataBase64;
    }

    /*
    public string Decrypt(string b64string)
    {
        //string b64string = "k0gR/jx16B70cFzN9Ur1Z/MG+l9vZcIh3xZ+TzTk0C9wiz57n/Vap8Jt0do3FFXDeM1aO2IFfB52ZIPGqOQAhQ==";

        // Create byte arrays to hold original, encrypted, and decrypted data.
        byte[] decryptedData;
        byte[] encryptedData;

        // decrypt with xml-private key
        _RSAalg.FromXmlString(_privateKey);

        encryptedData = Convert.FromBase64String(b64string);
        decryptedData = _RSAalg.Decrypt(encryptedData, false);

        return this.ByteArrayToString(decryptedData);
    }/**/


    private string ByteArrayToString(byte[] arr)
    {
        System.Text.ASCIIEncoding enc = new System.Text.ASCIIEncoding();
        return enc.GetString(arr);
    }
}