using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicTooltipController : MonoBehaviour
{
    public TMPro.TextMeshProUGUI labelLanguage;

    public void SetText(string s)
    {
        labelLanguage.text = s;
    }

}
