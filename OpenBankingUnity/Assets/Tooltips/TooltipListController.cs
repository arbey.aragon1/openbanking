using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TooltipListController : MonoBehaviour
{    
    private GameObject rotationalJoinGo;
    public List<GameObject> basicTooltips = new List<GameObject>();

    void Start()
    {

    }


    public void SetList(List<string> data)
    {
        float distanceDelta = 0.035f;
        float distance = 0.0f;

        rotationalJoinGo = Instantiate(Resources.Load<GameObject>("Tooltips/RotationalJoin"), gameObject.transform.position, Quaternion.identity);
        rotationalJoinGo.transform.SetParent(transform);

        foreach(string s in data){
            var pos = gameObject.transform.position + Vector3.up * distance;
            GameObject obj = Instantiate(Resources.Load<GameObject>("Tooltips/BasicTooltip"), pos, Quaternion.identity);
            obj.transform.SetParent(rotationalJoinGo.transform);
            obj.GetComponent<BasicTooltipController>().SetText(s);
            basicTooltips.Add(obj);
            distance += distanceDelta;
        }
    }

    public void SetActive(bool active)
    {
        this.gameObject.SetActive(active);
    }

}
