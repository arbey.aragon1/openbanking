using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TooltipController : MonoBehaviour
{
    public Transform origin;
    public Transform center;
    private GameObject tooltipList;
    private LineRenderer lineRender;
    private float lineDimension = 0.2f;

    public void ConfigureTooltip(List<string> data){
        var dir = (origin.position - center.position).normalized;

        var posLabel = origin.position + dir * lineDimension;

        tooltipList = Instantiate(Resources.Load<GameObject>("Tooltips/TooltipList"), posLabel, Quaternion.identity);
        tooltipList.transform.SetParent(transform);
        tooltipList.GetComponent<TooltipListController>().SetList(data);



        this.lineRender = tooltipList.AddComponent(typeof(LineRenderer)) as LineRenderer;
        
        this.lineRender.alignment = LineAlignment.View;
        this.lineRender.endColor = Color.white;
        this.lineRender.startColor = Color.white;
        this.lineRender.numCapVertices = 3;
        this.lineRender.numCornerVertices = 3;
        this.lineRender.useWorldSpace = false;
        this.lineRender.startWidth = 0.003f;
        //this.lineRender.material = SettingsController.GetLineTooltipsMat();
        
        this.lineRender.SetPosition(0, origin.position - tooltipList.transform.position);
        this.lineRender.SetPosition(1, posLabel - tooltipList.transform.position);

    }
}
