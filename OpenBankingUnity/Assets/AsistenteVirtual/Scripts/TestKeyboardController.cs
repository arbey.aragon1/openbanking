﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class TestKeyboardController : MonoBehaviour
{
    public AvatarController avatarController;
    public AudioController audioController;


    void Start()
    {
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.N))
        {
            Debug.Log("N");
            avatarController.UpdateStatus(1000f, 5000f, AvatarController.GetStatusValue("idle"));
        }
        if (Input.GetKeyDown(KeyCode.F))
        {
            Debug.Log("F");
            avatarController.UpdateStatus(1000f, 5000f, AvatarController.Status.Saludo1);
            audioController.LoadAudioClip("saludo1.mp3");
            audioController.Play((string s)=>{});
        }
        if (Input.GetKeyDown(KeyCode.I))
        {
            Debug.Log("I");
            avatarController.UpdateStatus(1000f, 5000f, AvatarController.Status.Saludo2);
            audioController.LoadAudioClip("saludo2.mp3");
            audioController.Play((string s)=>{});
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            Debug.Log("S");
            avatarController.UpdateStatus(1000f, 5000f, AvatarController.Status.Hablar1);
            audioController.LoadAudioClip("hablar1.mp3");
            audioController.Play((string s)=>{});
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            Debug.Log("P");
            avatarController.UpdateStatus(1000f, 5000f, AvatarController.Status.Hablar2);
            audioController.LoadAudioClip("hablar2.mp3");
            audioController.Play((string s)=>{});
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            Debug.Log("A");
            avatarController.UpdateStatus(1000f, 5000f, AvatarController.Status.Abrir);
            audioController.LoadAudioClip("abrir.mp3");
            audioController.Play((string s)=>{});
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            Debug.Log("E");
            avatarController.UpdateStatus(1000f, 5000f, AvatarController.Status.Escribir);
            audioController.LoadAudioClip("escribir.mp3");
            audioController.Play((string s)=>{});
        }
    }
}