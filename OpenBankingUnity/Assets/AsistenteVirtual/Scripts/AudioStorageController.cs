using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioStorageController : MonoBehaviour
{
    public AudioClip audioClip0;
    public AudioClip audioClip1;
    public AudioClip audioClip2;
    public AudioClip audioClip3;
    public AudioClip audioClip4;
    public AudioClip audioClip5;
    public AudioClip audioClip6;
    public AudioClip audioClip7;
    public AudioClip audioClip8;
    public AudioClip audioClip9;
    public AudioClip audioClip10;
    public AudioClip audioClip11;
    
    public AudioClip AudioClip(string key){

            if (key == "vo1") { return audioClip0; }
            else if (key == "vo2") { return audioClip1; }
            else if (key == "vo3") { return audioClip2; }
            else if (key == "vo4") { return audioClip3; }
            else if (key == "vo5") { return audioClip4; }
            else if (key == "vo6") { return audioClip5; }
            else if (key == "vo7") { return audioClip6; }
            else if (key == "vo8") { return audioClip7; }
            else if (key == "vo9") { return audioClip8; }
            else if (key == "vo10") { return audioClip9; }
            else if (key == "vo11") { return audioClip10; }
            else if (key == "vo12") { return audioClip11; }
        
        return null;
    }
}
