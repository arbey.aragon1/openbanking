using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using System.Threading.Tasks;
using SimpleJSON;

[RequireComponent(typeof(AudioSource))]
public class AudioController : MonoBehaviour
{
    public AudioStorageController audioStorageController;
    private AudioSource _audioSource;
    private string _path;

    void Start()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    public void SetAudioClip(AudioClip clip)
    {
        _audioSource.clip = clip;
        _audioSource.pitch = 1f;
    }

    public AudioClip GetAudioClip()
    {
        return _audioSource.clip;
    }

    public void Play(Action<string> callback)
    {
        StartCoroutine(PlaySound(callback));
    }

    private IEnumerator PlaySound(Action<string> callback)
    {
        _audioSource.Play();
        yield return new WaitForSeconds(_audioSource.clip.length);
        _audioSource.Stop();
        callback("");
    }

    public void LoadAudioClip(string audioName)
    {
        AudioClip audioClip = this.audioStorageController.AudioClip(audioName);
        GetComponent<AudioSource>().clip = audioClip;
        //GetComponent<AudioSource> ().Play ();
    }
}