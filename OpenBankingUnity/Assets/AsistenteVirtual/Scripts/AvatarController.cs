using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvatarController : MonoBehaviour
{
    public enum Status { Idle, Saludo1, Saludo2, Hablar1, Hablar2, Abrir, Escribir }

    private static Dictionary<string, Status> StatusDic = new Dictionary<string, Status> { 
            { "idle", Status.Idle },
            { "saludo1", Status.Saludo1 },
            { "saludo2", Status.Saludo2 },
            { "hablar1", Status.Hablar1 },
            { "hablar2", Status.Hablar2 },
            { "abrir", Status.Abrir},
            { "escribir", Status.Escribir },
        };
    private float startAnim;
    private float endAnim;
    private Status status;
    private Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }
    public static Status GetStatusValue(string value)
    {
        Debug.Log(value);
        return StatusDic[value];
    }
    public void UpdateStatus(float startAnim, float endAnim, Status st)
    {
        this.startAnim = startAnim;
        this.endAnim = endAnim;
        this.status = st;
        //StopCoroutine ("StartAnim");
        StartCoroutine("StartAnim");
        //StopCoroutine ("EndAnim");
        StartCoroutine("EndAnim");
    }
    IEnumerator StartAnim()
    {
        yield return new WaitForSeconds(this.startAnim / 1000f);
        if (this.status == Status.Idle)
        {
            anim.SetInteger("status", 0);
        }
        else if (this.status == Status.Saludo1)
        {
            anim.SetInteger("status", 1);
        }
        else if (this.status == Status.Saludo2)
        {
            anim.SetInteger("status", 2);
        }
        else if (this.status == Status.Hablar1)
        {
            anim.SetInteger("status", 3);
        }
        else if (this.status == Status.Hablar2)
        {
            anim.SetInteger("status", 4);
        }
        else if (this.status == Status.Abrir)
        {
            anim.SetInteger("status", 5);
        }
        else if (this.status == Status.Escribir)
        {
            anim.SetInteger("status", 6);
        }
    }
    IEnumerator EndAnim()
    {
        yield return new WaitForSeconds(this.endAnim / 1000f);
        anim.SetInteger("status", 0);
    }

    public void SetActive(bool v){
        gameObject.SetActive(v);
    }
}
