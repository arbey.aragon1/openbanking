
import { expect } from "chai";
import { EnvironmentService } from "../../src/services/environmentService";
import { AsymmetricEncryptionService } from "../../src/services/asymmetricEncryptionService";

describe('Test asymmetric service: ', function() {
    it('Should be able to encrypt and decrypt the message getting the same input result', function() {
        const input = 'Hello world!'
        const environmentService = new EnvironmentService();
        const asymmetricEncryptionService = new AsymmetricEncryptionService(
          environmentService
        );
        const enc = asymmetricEncryptionService.Encrypt(input)
        const dec = asymmetricEncryptionService.Decrypt(enc)
        expect(input).to.equal(dec);
    });
});

