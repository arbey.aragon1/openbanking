
import { expect } from "chai";
import { SymmetricEncryptionService } from "../../src/services/symmetricEncryptionService";

describe('Test symmetric service: ', function() {
    it('Should be able to encrypt and decrypt the message getting the same input result', function() {
        const input = 'Hello world!'
        const asymmetricEncryptionService = new SymmetricEncryptionService(
        );
        const enc = asymmetricEncryptionService.Encrypt(input, "C64F8DAAA4F230C25AD122847ED28FB245D5BB2F99E6F93787FE0775798F92A2")
        const dec = asymmetricEncryptionService.Decrypt(enc, "C64F8DAAA4F230C25AD122847ED28FB245D5BB2F99E6F93787FE0775798F92A2")
        console.log(input);
        console.log(dec);
        
        expect(input).to.equal(dec);
    });
});

