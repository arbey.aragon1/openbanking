import express from "express";
import firebase from "firebase";
import * as admin from "firebase-admin";
import { Observable, from, of } from "rxjs";
import { tap, map, switchMap, concatMapTo, combineAll } from "rxjs/operators";
import { combineLatest } from "rxjs";
import crypto from "crypto";
var fs = require("fs");

import { Module } from "./services/module";
import { FirebaseService } from "./services/firebaseService";
import { EnvironmentService } from "./services/environmentService";
import { UserService } from "./services/userService";
import { SymmetricEncryptionService } from "./services/symmetricEncryptionService";
import { AsymmetricEncryptionService } from "./services/asymmetricEncryptionService";
import { KeyChainService } from "./services/keyChainService";
import { PrometeoService } from "./services/prometeoService";
const path = require("path");

const moduleInst = Module.getInstance();

interface MsgMs {
  contract: string;
  uidSender: string;
  token: string;
  key: string;
  command: string;
  payload: any;
  commited: boolean;
  validToken: boolean;
  uidInToken: string;
}

interface DataOfUser {
  status?: string;
  providers?: any;
  accounts?: any;
}

export class App {
  private _module: Module;

  private _firebaseService: FirebaseService;
  private _environmentService: EnvironmentService;
  private _userService: UserService;
  private _symmetricEncryptionService: SymmetricEncryptionService;
  private _asymmetricEncryptionService: AsymmetricEncryptionService;
  private _keyChainService: KeyChainService;
  private _prometeoService: PrometeoService;

  public constructor(module: Module) {
    this._module = module;
    this._firebaseService = moduleInst.FirebaseService();
    this._asymmetricEncryptionService =
      moduleInst.AsymmetricEncryptionService();
    this._symmetricEncryptionService = moduleInst.SymmetricEncryptionService();
    this._environmentService = moduleInst.EnvironmentService();
    this._userService = moduleInst.UserService();
    this._keyChainService = moduleInst.KeyChainService();
    this._prometeoService = moduleInst.PrometeoService();
  }

  public Pipeline() {
    return of({}).pipe(
      switchMap(() => {
        return this._firebaseService.FetchFromPath2("/ms-auth");
      }),
      map((val) => {
        //Data manipulation
        const value: MsgMs = {
          contract: "",
          uidSender: val.value.uid,
          token: val.value.token,
          key: val.key,
          command: val.value.command,
          payload: val.value,
          commited: false,
          validToken: false,
          uidInToken: "",
        };
        return value;
      }),
      switchMap((value) => {
        //token validation
        return this._firebaseService
          .TokenVerification(value.token, value.uidSender)
          .pipe(
            map((v) => {
              value.validToken = v.valid;
              value.uidInToken = v.uid;
              return value;
            })
          );
      }),
      switchMap((value) => {
        return this.CommandSelector(value);
      })
    );
  }

  private CommandSelector(value) {
    //Update data
    if (value.validToken && value.command == "START_CONNECTION") {
      return this.StartConnection(value);
    } else if (value.validToken && value.command == "SYNC_KEYS") {
      return this.SyncKeys(value);
    } else if (value.validToken && value.command == "AUTH_PROMETEO") {
      return this.AuthPrometeo(value);
    } else if (value.validToken && value.command == "PROVIDER_LIST") {
      return this.ProviderList(value);
    } else if (value.validToken && value.command == "ACCOUNT_LIST") {
      return this.AccountList(value);
    } else if (value.validToken && value.command == "ACCOUNT_MOVEMENT") {
      return this.AccountMovement(value);
    }
    return of(value);
  }

  private StartConnection(value) {
    return this._firebaseService
      .SendResponse("ms-auth-response", value.uidSender, value.key, {
        msg: this._asymmetricEncryptionService.GetRSAPublicXML(),
      })
      .pipe(
        switchMap((v) => {
          return this._firebaseService.DeleteAndReturnValue(
            "/ms-auth/" + value.key,
            value
          );
        })
      );
  }

  private SyncKeys(value) {
    console.log(value.payload.msg);
    const decryptedKey = this._asymmetricEncryptionService.Decrypt(
      value.payload.msg
    );
    this._keyChainService.SaveKeySymmetricEnc(value.uidSender, decryptedKey);

    /*
    const msg = this._symmetricEncryptionService.Encrypt(
      "Hi!",
      this._keyChainService.GetKeySymmetricEnc(value.uidSender)
    );

    return this._firebaseService
      .SendResponse("ms-auth-response", value.uidSender, value.key, {
        msg: msg,
      })
      .pipe(
        switchMap((v) => {
          console.log(value);
          return this._firebaseService.DeleteAndReturnValue(
            "/ms-auth/" + value.key,
            value
          );
        })
      );

    /** */

    return this.GetAll().pipe(
      switchMap((data) => {
        const msg = this._symmetricEncryptionService.Encrypt(
          data,
          this._keyChainService.GetKeySymmetricEnc(value.uidSender)
        );
        return this._firebaseService
          .SendResponse("ms-auth-response", value.uidSender, value.key, {
            msg: msg,
          })
          .pipe(
            switchMap((v) => {
              console.log(value);
              return this._firebaseService.DeleteAndReturnValue(
                "/ms-auth/" + value.key,
                value
              );
            })
          );
      })
    );/** */
  }

  private AuthPrometeo(value) {
    return of(value);
  }

  private ProviderList(value) {
    return of(value);
  }

  private AccountList(value) {
    return of(value);
  }

  private AccountMovement(value) {
    return of(value);
  }

  public GetAll() {
    const uid = "1111";
    const user = "12345";
    const pass = "gfdsa&type=";

    return this._prometeoService.Login(uid, user, pass).pipe(
      map((value) => {
        const data: DataOfUser = {
          status: value["status"],
        };
        return data;
      }),
      switchMap((value: DataOfUser) => {
        return this._prometeoService.ProviderList().pipe(
          map((v) => {
            value.providers = v["providers"];
            return value;
          })
        );
      }),
      switchMap((value: DataOfUser) => {
        return this._prometeoService.AccountList(uid).pipe(
          switchMap((v) => {
            console.log("---------------------------------");
            console.log(v);

            var obs = v["accounts"].map((element) => {
              return this._prometeoService.AccountMovement(
                uid,
                element.number,
                element.currency,
                element
              );
            });
            return combineLatest(obs).pipe(
              map((va) => {
                value.accounts = va;
                const out = JSON.stringify(value);
                /*try {
                  console.log(path.join(__dirname,'json.json'));
                  fs.writeFileSync(path.join(__dirname,'json.json'), out)
                } catch (err) {
                  console.error(err)
                }/** */
                return out;
              })
            );
          })
        );
      })
    );
  }
}

const app = new App(moduleInst);

//app.GetAll();

app.Pipeline().subscribe(
  (val) => {
    console.log(val);
  },
  (err) => {
    console.log("Error");
    console.log(err);
  }
);
/** */
