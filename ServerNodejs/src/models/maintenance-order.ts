export class MaintenanceOrder {
    constructor(
        private _key: string,
        private _status: number,
        private _description: string
    ) {}

    get key(): string {
        return this._key;
    }

    get status(): number {
        return this._status;
    }

    get description(): string {
        return this._description;
    }

    public static fromJSON({
        key,
        status,
        description,
    }: any): MaintenanceOrder {
        return new MaintenanceOrder(key, status, description);
    }

    public static fromJSONArray(array: any[]): MaintenanceOrder[] {
        return array
            .map((value: any) => {
                return { uid: value.key, ...value.payload.val() };
            })
            .map(MaintenanceOrder.fromJSON);
    }

    /**
     * README:
     * toJSON no utiliza el key del usuario
     * Se debe utilizar este método para guardar el usuario en
     * firebase con el método push
     */
    public toJSON(): any {
        return {
            status: this.status,
            description: this.description,
        };
    }
}
