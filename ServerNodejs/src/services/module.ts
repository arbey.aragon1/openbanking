import { FirebaseService } from "./firebaseService";
import { EnvironmentService } from "./environmentService";
import { UserService } from "./userService";
import { SymmetricEncryptionService } from "./symmetricEncryptionService";
import { AsymmetricEncryptionService } from "./asymmetricEncryptionService";
import { KeyChainService } from "./keyChainService";
import { PrometeoService } from "./prometeoService";

export class Module {
  private static instance: Module;
  private _firebaseService: FirebaseService;
  private _environmentService: EnvironmentService;
  private _userService: UserService;
  private _symmetricEncryptionService: SymmetricEncryptionService;
  private _asymmetricEncryptionService: AsymmetricEncryptionService;
  private _keyChainService: KeyChainService;
  private _prometeoService: PrometeoService;

  private constructor() {
    this._environmentService = new EnvironmentService();
    this._firebaseService = new FirebaseService(
      this._environmentService
    );
    this._symmetricEncryptionService = new SymmetricEncryptionService();
    this._asymmetricEncryptionService = new AsymmetricEncryptionService(
      this._environmentService
    );
    this._keyChainService = new KeyChainService(this._environmentService);
    this._prometeoService = new PrometeoService(this._environmentService, this._keyChainService);
  }

  public static getInstance(): Module {
    if (!Module.instance) {
      Module.instance = new Module();
    }
    return Module.instance;
  }

  public FirebaseService(): FirebaseService {
    return this._firebaseService;
  }

  public EnvironmentService(): EnvironmentService {
    return this._environmentService;
  }

  public UserService(): UserService {
    return this._userService;
  }

  public SymmetricEncryptionService(): SymmetricEncryptionService {
    return this._symmetricEncryptionService;
  }

  public AsymmetricEncryptionService(): AsymmetricEncryptionService {
    return this._asymmetricEncryptionService;
  }

  public KeyChainService(): KeyChainService{
    return this._keyChainService;
  }

  public PrometeoService(): PrometeoService{
    return this._prometeoService;
  }
}
