import * as crypto from "crypto";
import * as path from "path";
import * as fs from "fs";
import { EnvironmentService } from "./environmentService";

export class AsymmetricEncryptionService {
  private _environmentService: EnvironmentService;
  private readonly IV_SIZE: number = 16;

  public constructor(environmentService: EnvironmentService) {
    this._environmentService = environmentService;
  }

  public Encrypt(toEncrypt): string {
    const relativeOrAbsolutePathToPublicKey = path.join(
      this._environmentService.CREDANTEIALS_PATH,
      "public.pem"
    );
    const absolutePath = path.resolve(relativeOrAbsolutePathToPublicKey);
    const publicKey = fs.readFileSync(absolutePath, "utf8");
    const buffer = Buffer.from(toEncrypt, "utf8");
    var constants = require("constants");
    const encrypted = crypto.publicEncrypt(
      {
        key: publicKey,
        padding: constants.RSA_PKCS1_PADDING,
      },
      buffer
    );
    return encrypted.toString("base64");
  }

  public Decrypt(toDecrypt): string {
    const relativeOrAbsolutePathtoPrivateKey = path.join(
      this._environmentService.CREDANTEIALS_PATH,
      "private.pem"
    );
    const absolutePath = path.resolve(relativeOrAbsolutePathtoPrivateKey);
    const privateKey = fs.readFileSync(absolutePath, "utf8").toString();
    const buffer = Buffer.from(toDecrypt, "base64");
    var constants = require("constants");
    const decrypted = crypto.privateDecrypt(
      {
        key: privateKey,
        passphrase: "",
        padding: constants.RSA_PKCS1_PADDING,
      },
      buffer
    );
    return decrypted.toString("utf8");
  }

  public GetRSAPublicXML(): string{
    const relativeOrAbsolutePathToPublicKey = path.join(
      this._environmentService.CREDANTEIALS_PATH,
      "public.xml"
    );
    const absolutePath = path.resolve(relativeOrAbsolutePathToPublicKey);
    const publicKey = fs.readFileSync(absolutePath, "utf8").toString();
    console.log(relativeOrAbsolutePathToPublicKey);
    
    return publicKey;
  }
}
