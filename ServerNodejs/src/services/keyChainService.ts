import { EnvironmentService } from "./environmentService";

export class KeyChainService {
    private static instance: KeyChainService;
    private _environmentService: EnvironmentService;
    private keyChain = {};
  
    public constructor(environmentService: EnvironmentService) {
      this._environmentService = environmentService;
    }

    public SaveUserAndPass(uid: string, user: string, pass: string){
        this.keyChain[uid] = {
            ...this.keyChain[uid],
            'user': user,
            'pass': pass,
        } 
    }

    public SaveLoginToken(uid: string, token: string, statusLogin: string){
        this.keyChain[uid] = {
            ...this.keyChain[uid],
            'token': token,
            'statusLogin': statusLogin
        } 
    }

    public SaveKeySymmetricEnc(uid: string, key: string){
        this.keyChain[uid] = {
            ...this.keyChain[uid],
            'key': key,
        } 
    }

    public GetKeySymmetricEnc(uid: string){
        return this.keyChain[uid]['key']
    }

    public GetToken(uid: string){
        return this.keyChain[uid]['token']
    }
}