const dotenv = require("dotenv");
dotenv.config();
const path = require("path");
export class EnvironmentService {
  private static instance: EnvironmentService;
  public readonly ENVIRONMENT: string;
  public readonly CREDANTEIALS_PATH: string;
  public readonly DATABASE_PATH: string;
  public readonly API_KEY_PROMETEO: string;
  public readonly URL_API_PROMETEO: string;

  public constructor() {
    this.ENVIRONMENT = process.env.ENVIRONMENT;
    this.CREDANTEIALS_PATH = path.join(
      __dirname.split("ServerNodejs")[0],
      "ServerNodejs",
      "credentials",
    );
    this.DATABASE_PATH = process.env.DATABASE_PATH;
    this.API_KEY_PROMETEO = process.env.API_KEY_PROMETEO;
    this.URL_API_PROMETEO = process.env.URL_API_PROMETEO;
  }

}
