import { Observable, from, of } from "rxjs";
import { tap, map, switchMap } from "rxjs/operators";
import * as admin from "firebase-admin";
import { EnvironmentService } from "./environmentService";
const path = require("path");
var fs = require("fs");

export class FirebaseService {
    private static instance: FirebaseService;
    public adminRef: any = null;
    private _dbReference: any = null;
    private _environmentService: EnvironmentService;

    public constructor(environmentService: EnvironmentService) {
        this._environmentService = environmentService;

        var obj = JSON.parse(
            fs.readFileSync(
                path.join(this._environmentService.CREDANTEIALS_PATH, "gcp-credential.json"),
                "utf8"
            )
        );
        admin.initializeApp({
            credential: admin.credential.cert({
                projectId: obj.project_id,
                privateKey: obj.private_key?.replace(/\\n/g, "\n"),
                clientEmail: obj.client_email,
            }),
            databaseURL: this._environmentService.DATABASE_PATH,
        });
        this._dbReference = admin.database();
        this.adminRef = admin;
    }

    public FetchFromPath(path: string): Observable<any> {
        return new Observable((observer) => {
            this._dbReference.ref(path).on(
                "value",
                (snapshot) => {
                    observer.next({ key: snapshot.key, ...snapshot.val() });
                    // observer.complete();
                },
                (err) => {
                    observer.error(err);
                }
            );
        });
    }

    public FetchFromPath2(path: string): Observable<any> {
        return new Observable((observer) => {
            this._dbReference
            .ref(path)
            .limitToLast(1)
            .on(
                "value",
                (snapshot) => {
                    if (snapshot.val() != null) {
                        console.log(snapshot.val());
                        const obj = snapshot.val();
                        const key = Object.keys(obj)[0];

                        observer.next({ key: key, value: obj[key] });
                        // observer.complete();
                    }
                },
                (err) => {
                    observer.error(err);
                }
            );
        });
    }

    public FetchFromPathFirtered(path: string): Observable<any> {
        return new Observable((observer) => {
            this._dbReference
                .ref(path)
                .orderByChild("msSelected")
                .limitToLast(1)
                .on(
                    "value",
                    (snapshot) => {
                        if (snapshot.val() != null) {
                            const obj = snapshot.val();
                            const key = Object.keys(obj)[0];

                            if (obj[key].msSelected == 2) {
                                observer.next({ key: key, value: obj[key] });
                            }
                            // observer.complete();
                        }
                    },
                    (err) => {
                        observer.error(err);
                    }
                );
        });
    }

    public FetchOnceFromPath(path: string): Observable<any> {
        return new Observable((observer) => {
            this._dbReference.ref(path).once(
                "value",
                (snapshot) => {
                    observer.next({ key: snapshot.key, ...snapshot.val() });
                    observer.complete();
                },
                (err) => {
                    observer.error(err);
                }
            );
        });
    }

    public TokenDecode(token: string): Observable<any> {
        return from(admin.auth().verifyIdToken(token));
    }

    public TokenVerification(token: string, uid: string): Observable<any> {
        return this.TokenDecode(token).pipe(
            map((decodedToken) => {
                const valid = uid == decodedToken.uid
                
                
                return {
                    uid: decodedToken.uid,
                    valid: valid,
                };
            })
        );
    }

    public FetchWithTransaction(path: string): Observable<any> {
        var updated = false;
        console.log(path);

        return from(
            this._dbReference.ref(path + "/msSelected").transaction((value) => {
                if (value == 2) {
                    // the counter doesn't exist yet, start at one
                    updated = true;
                    return 1;
                } else {
                    // increment - the normal case
                    updated = false;
                    return;
                }
            })
        ).pipe(
            switchMap(() => {
                if (updated) {
                    return this.FetchOnceFromPath(path).pipe(
                        map((v) => ({ updated: updated, value: v }))
                    );
                } else {
                    return of({ updated: updated, value: {} });
                }
            })
        );
    }

    public WriteBatch() {
        const batch = this._dbReference.batch();
    }

    public Delete(path: string): Observable<any> {
        return from(this._dbReference.ref(path).remove());
    }

    public DeleteAndReturnValue(path: string, value: any): Observable<any> {
        return from(this._dbReference.ref(path).remove()).pipe(
            map((v) => {
              return value;
            })
          );
    }

    public SendResponse(path: string, uid: string, key: string, object: any): Observable<any> {
        return from(this._dbReference.ref('/'+path+'/'+uid+'/'+key).set(object));
    }
}
