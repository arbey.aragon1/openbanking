import request from "request";
import * as path from "path";
import { EnvironmentService } from "./environmentService";
import { KeyChainService } from "./keyChainService";
import { Observable, from, of } from "rxjs";
import { tap, map, switchMap } from "rxjs/operators";

function callback(error, response, body) {
  //console.log(body);
  if (!error && response.statusCode == 200) {
    //console.log(body);
  } else {
    console.log(body);
  }
}

export class PrometeoService {
  private _environmentService: EnvironmentService;
  private _keyChainService: KeyChainService;

  public constructor(
    environmentService: EnvironmentService,
    keyChainService: KeyChainService
  ) {
    this._environmentService = environmentService;
    this._keyChainService = keyChainService;
  }

  public Login(uid: string, username: string, password: string) {
    var dataString =
      "provider=test&username=" + username + "&password=" + password;
    const headers = {
      accept: "application/json",
      "X-API-Key": this._environmentService.API_KEY_PROMETEO,
      "Content-Type": "application/x-www-form-urlencoded",
    };
    var options = {
      url: this._environmentService.URL_API_PROMETEO + "login/",
      method: "POST",
      headers: headers,
      body: dataString,
    };
    return new Observable((observer) => {
      request(options, (error, response, body) => {
        if (!error && response.statusCode == 200) {
          var jsonObject = JSON.parse(body);
          this._keyChainService.SaveUserAndPass(uid, username, password);
          this._keyChainService.SaveLoginToken(
            uid,
            jsonObject.key,
            jsonObject.status
          );
          observer.next(jsonObject);
        } else {
          observer.next({ msg: error });
        }
      });
    });
  }

  public ProviderList() {
    var headers = {
      accept: "application/json",
      "X-API-Key": this._environmentService.API_KEY_PROMETEO,
    };
    var options = {
      url: this._environmentService.URL_API_PROMETEO + "provider/",
      headers: headers,
    };

    return new Observable((observer) => {
      request(options, (error, response, body) => {
        if (!error && response.statusCode == 200) {
          var jsonObject = JSON.parse(body);
          observer.next(jsonObject);
        } else {
          observer.next({ msg: error });
        }
      });
    });
  }

  public AccountList(uid: string) {
    var headers = {
      accept: "application/json",
      "X-API-Key": this._environmentService.API_KEY_PROMETEO,
    };

    var options = {
      url:
        this._environmentService.URL_API_PROMETEO +
        "account/?key=" +
        this._keyChainService.GetToken(uid),
      headers: headers,
    };
    return new Observable((observer) => {
        request(options, (error, response, body) => {
          if (!error && response.statusCode == 200) {
            var jsonObject = JSON.parse(body);
            observer.next(jsonObject);
          } else {
            observer.next({ msg: error });
          }
        });
      });
  }

  public AccountMovement(uid: string, account: string, currency: string, element: any) {
    const date_start = "17%2F11%2F2021";
    const date_end = "18%2F11%2F2021";
    var headers = {
      accept: "application/json",
      "X-API-Key": this._environmentService.API_KEY_PROMETEO,
    };

    var options = {
      url:
        this._environmentService.URL_API_PROMETEO +
        "account/" +
        account +
        "/movement/?currency=" +
        currency +
        "&date_start=" +
        date_start +
        "&date_end=" +
        date_end +
        "&key=" +
        this._keyChainService.GetToken(uid),
      headers: headers,
    };
    return new Observable((observer) => {
        request(options, (error, response, body) => {
          if (!error && response.statusCode == 200) {
            var jsonObject = JSON.parse(body);
            element['movements'] = jsonObject['movements']
            observer.next(element);
          } else {
            element['movements'] = { msg: error }
            observer.next(element);
          }
        });
      });
  }
}
