import { createCipher, createDecipher }  from "crypto";

export class SymmetricEncryptionService {
  private static instance: SymmetricEncryptionService;
  private readonly IV_SIZE: number = 16;

  public constructor() {}


  public Encrypt(plainText, keyString) {
    console.log(keyString);
    
    var cipher = createCipher("aes-256-cbc", keyString);
    var encrypted =
      cipher.update(plainText, "utf8", "base64") + cipher.final("base64");
    return encrypted;
    /*let iv = crypto.randomBytes(IV_SIZE);
        let cipher = crypto.createCipheriv("aes-256-cbc", keyString, iv);
        let encrypted = cipher.update(plainText);
        encrypted = Buffer.concat([encrypted, cipher.final()]);
        return iv.toString('hex') + ':' + encrypted.toString('hex');/** */
  }

  public Decrypt(combinedString, keyString) {
    var decipher = createDecipher("aes-256-cbc", keyString);
    var plain =
      decipher.update(combinedString, "base64", "utf8") +
      decipher.final("utf8");
    return plain;
    /*let textParts = combinedString.split(':');
        let iv = Buffer.from(textParts.shift(), 'hex');
        let encryptedText = Buffer.from(textParts.join(':'), 'hex');
        let decipher = crypto.createDecipheriv("aes-256-cbc", keyString, iv);
        let decrypted = decipher.update(encryptedText);
        decrypted = Buffer.concat([decrypted, decipher.final()]);
        return decrypted.toString();/** */
  }
}
